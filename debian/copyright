Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: mousai
Upstream-Contact: Dave Patrick Caberto <davecruz48@gmail.com>
Source: https://github.com/SeaDve/Mousai

Files: *
Copyright: Dave Patrick Caberto <davecruz48@gmail.com>
License: GPL-3+

Files: src/song_filter.rs
       src/song_sorter.rs
Copyright: 2022 John Toohey <john_t@mailo.com>
           2023 Dave Patrick Caberto
License: GPL-3+

Files: src/i18n.rs 
Copyright: fractal contributors 
License: GPL-3+
Comment: See https://gitlab.gnome.org/GNOME/fractal/-/blob/c0bc4078bb2cdd511c89fdf41a51275db90bb7ab/src/i18n.rs

Files: data/io.github.seadve.Mousai.metainfo.xml*
Copyright: Dave Patrick Caberto <davecruz48@gmail.com>
License: CC0-1.0

Files: debian/*
Copyright: 2024 Matthias Geiger <werdahias@riseup.net>
           2024 Chris Talbot <chris@talbothome.com> 
License: GPL-3+

License: GPL-3+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-3".

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all
 copyright and related and neighboring rights to this software to the
 public domain worldwide. This software is distributed without any
 warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication
 along with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 Public Domain
 Dedication can be found in `/usr/share/common-licenses/CC0-1.0’.
